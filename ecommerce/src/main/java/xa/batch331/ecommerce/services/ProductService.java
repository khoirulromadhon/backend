package xa.batch331.ecommerce.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.ecommerce.models.Product;
import xa.batch331.ecommerce.models.ProductDTO;
import xa.batch331.ecommerce.repositories.ProductRepo;

import java.util.List;
import java.util.Map;

@Service
public class ProductService {
    @Autowired
    private ProductRepo productRepo;

    // Get All Product
    public List<Product> getAllProduct() {
        return this.productRepo.findAll();
    }

    // Get Product By Id
    public Product getProductById(Long id){
        return this.productRepo.findById(id).orElse(null);
    }

    // Store Data
    public void storeProduct(Product product) {
        this.productRepo.save(product);
    }

    // Delete Product
    public void deleteProduct(Long id){
        this.productRepo.deleteById(id);
    }

    // Convert Entiti ke DTO
    public ProductDTO convertToDTO(Product product){
        ModelMapper modelMapper = new ModelMapper();
        ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
        return productDTO;
    }

    // Find By Id with DTO
    public ProductDTO findProductByIdDTO(Long id){
        Product product = this.productRepo.findById(id).orElse(null);
        return convertToDTO(product);
    }

    // Get All with Hash Map
    public List<Map<String, Object>> getProductWithHashMap(){return this.productRepo.getAllProducts();}
}
