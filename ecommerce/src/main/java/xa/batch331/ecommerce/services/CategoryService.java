package xa.batch331.ecommerce.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.ecommerce.models.Category;
import xa.batch331.ecommerce.models.CategoryDTO;
import xa.batch331.ecommerce.repositories.CategoryRepo;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepo categoryRepo;

    // Get All
    public List<Category> getAllCategory(){
        return this.categoryRepo.findAll();
    }

    // Find By Id
    public Optional<Category> getCategoryById(Long id){
        return this.categoryRepo.findById(id);
    }

    // Store Data
    public void SimpanCategory(Category category) {
        this.categoryRepo.save(category);
    }

    // Find By id tapi cara lain
    public Category getCategoryByIdDua(Long id){
        return this.categoryRepo.findById(id).orElse(null);
    }

    // Convert Entity ke DTO
    public CategoryDTO convertToDTO(Category category){
        ModelMapper modelMapper = new ModelMapper();
        CategoryDTO categoryDTO = modelMapper.map(category, CategoryDTO.class);
        return categoryDTO;
    }

    // Find By id dengan DTO
    public CategoryDTO categoryDTOById(Long id){
        Category category = this.categoryRepo.findById(id).orElse(null);
        return convertToDTO(category);
    }

    // Get All with Hash Map
    public List<Map<String, Object>> getCategoryValue(){
        return this.categoryRepo.getCategoryValue();
    }

    // Delete
    public void deleteCategory(Long id){
         this.categoryRepo.deleteById(id);
    }

    // Images REST
    public byte[] readFileContent(Long id){
        Category category = this.categoryRepo.findById(id).orElse(null);
        if (category != null){
            return category.getFileContent();
        }
        else {
            return null;
        }
    }
}
