package xa.batch331.ecommerce.models;

public class ProductDTO {
    private String Name;

    private Long CategoryId;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Long categoryId) {
        CategoryId = categoryId;
    }
}
