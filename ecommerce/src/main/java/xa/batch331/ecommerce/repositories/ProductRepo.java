package xa.batch331.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.batch331.ecommerce.models.Product;

import java.util.List;
import java.util.Map;

public interface ProductRepo extends JpaRepository<Product, Long> {
    @Query(value = "SELECT p.name, p.category_id FROM products p", nativeQuery = true)
    List<Map<String, Object>> getAllProducts();
}
