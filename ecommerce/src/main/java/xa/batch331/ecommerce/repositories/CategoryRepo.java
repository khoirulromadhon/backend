package xa.batch331.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import xa.batch331.ecommerce.models.Category;

import java.util.List;
import java.util.Map;

public interface CategoryRepo extends JpaRepository<Category, Long> {
    @Query(value = "SELECT c.name, c.description FROM categories c", nativeQuery = true)
    List<Map<String, Object>> getCategoryValue();
}
