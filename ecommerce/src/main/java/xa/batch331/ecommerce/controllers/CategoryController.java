package xa.batch331.ecommerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import xa.batch331.ecommerce.models.Category;
import xa.batch331.ecommerce.services.CategoryService;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping
    public ModelAndView getCategory(){
        ModelAndView view = new ModelAndView("category/index");
        List<Category> categories = this.categoryService.getAllCategory();
        view.addObject("categoryData", categories);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getCategoryById(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/details");
        Optional<Category> category = this.categoryService.getCategoryById(id);
        view.addObject("category", category.get());
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("category/form");
        Category category = new Category();
        view.addObject("category", category);
        return view;
    }

    private static String UPLOADED_FOLDER = "E://Pembekalan/Backend/ecommerce/src/main/resources/img/";

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute Category category, BindingResult result, @RequestParam("filepath")MultipartFile file) throws Exception{
        if (!result.hasErrors()){
            category.setFilePath("/files/" + file.getOriginalFilename());
            this.categoryService.SimpanCategory(category);

            try{
                if (file.getOriginalFilename() != ""){
                    // save to local
//                    byte[] bytes = file.getBytes();
                    Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
//                    Files.write(path, bytes);

                    // save to byte
                    byte[] fileContent = file.getBytes();
//                    category.setFileContent(fileContent);

                    category.setFilePath("/files/" + file.getOriginalFilename());
                    category.setFileContent(fileContent);
                    this.categoryService.SimpanCategory(category);
//                    Path path1 = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                    Files.write(path, fileContent);
                }
            }
            catch (Exception exception){
                exception.printStackTrace();
            }
        }
        return new ModelAndView("redirect:/category");
    }

    @GetMapping("/editform/{id}")
    public ModelAndView editForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/form");
        Category category = this.categoryService.getCategoryByIdDua(id);
        view.addObject("category", category);
        return view;
    }

    @GetMapping("/deleteform/{id}")
    public ModelAndView deleteForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/delete");
        Category category = this.categoryService.getCategoryByIdDua(id);
        view.addObject("category", category);
        return view;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView getMethodName(@PathVariable("id") Long id){
        this.categoryService.deleteCategory(id);
        return new ModelAndView("redirect:/category");
    }

    @GetMapping("getByteFile/{id}")
    public ModelAndView getByteFile(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("category/getfile");
        byte[] fileContent = this.categoryService.readFileContent(null);
        view.addObject("fileContent", fileContent);
        return view;
    }
}
