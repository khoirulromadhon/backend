package xa.batch331.ecommerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import xa.batch331.ecommerce.models.Category;
import xa.batch331.ecommerce.models.Product;
import xa.batch331.ecommerce.services.CategoryService;
import xa.batch331.ecommerce.services.ProductService;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @GetMapping
    public ModelAndView getAllProducts(){
        ModelAndView view = new ModelAndView("product/index");
        List<Product> products = this.productService.getAllProduct();
        view.addObject("products", products);
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("product/form");
        Product product = new Product();
        List<Category> categories = this.categoryService.getAllCategory();
        view.addObject("product", product);
        view.addObject("categories", categories);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute Product product, BindingResult result){
        this.productService.storeProduct(product);
        return new ModelAndView("redirect:/product");
    }
}
