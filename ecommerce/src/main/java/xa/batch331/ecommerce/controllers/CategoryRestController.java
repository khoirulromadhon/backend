package xa.batch331.ecommerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.ecommerce.models.Category;
import xa.batch331.ecommerce.services.CategoryService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CategoryRestController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("category")
    public ResponseEntity<List<Category>> getAllCategory(){
        try {
            List<Category> categories = this.categoryService.getAllCategory();
            return new ResponseEntity<>(categories, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("categoryValue")
    public ResponseEntity<List<Map<String, Object>>> getCategoryValue(){
        try {
            List<Map<String, Object>> categories = this.categoryService.getCategoryValue();
            return new ResponseEntity<List<Map<String, Object>>>(categories, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("category/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable("id") Long id){
        try{
            Category category = this.categoryService.getCategoryByIdDua(id);
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("category")
    public ResponseEntity<Category> saveCategory(@RequestBody Category category){
        try {
            this.categoryService.SimpanCategory(category);
            return new ResponseEntity<Category>(category, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("category/{id}")
    public ResponseEntity<?> updateCategory(@RequestBody Category category, @PathVariable("id") Long id){
        try {
            category.setId(id);
            this.categoryService.SimpanCategory(category);
            Map<String, Object> result = new HashMap<>();
            String message = "Edit data berhasil (OK - 200)";
            result.put("Message", message);
            result.put("Data", category);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("category/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Long id){
        try{
            Category category = this.categoryService.getCategoryByIdDua(id);
            if (category != null){
                this.categoryService.deleteCategory(id);
                return ResponseEntity.status(HttpStatus.OK).body("Category deleted");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Category not found");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //DTO

}
