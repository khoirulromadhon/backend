package xa.batch331.ecommerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.ecommerce.models.Product;
import xa.batch331.ecommerce.services.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ProductRestController {
    @Autowired
    private ProductService productService;

    @GetMapping("product")
    public ResponseEntity<List<Product>> getAllproducts(){
        try {
            List<Product> products = this.productService.getAllProduct();
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("product/{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") Long id){
        try{
            Product product = this.productService.getProductById(id);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("product")
    public ResponseEntity<Product> saveProduct(@RequestBody Product product){
        try{
            this.productService.storeProduct(product);
            return new ResponseEntity<Product>(product, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("product/{id}")
    public ResponseEntity<?> updateProduct(@RequestBody Product product, @PathVariable("id") Long id){
        try{
            product.setId(id);
            this.productService.storeProduct(product);
            Map<String, Object> result = new HashMap<>();
            String message = "Update success !!!!!";
            result.put("Message", message);
            result.put("Data", product);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("product/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Long id){
        try{
            Product product = this.productService.getProductById(id);
            if (product != null){
                this.productService.deleteProduct(id);
                return ResponseEntity.status(HttpStatus.OK).body("Product Deleted");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Product Not Found");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
